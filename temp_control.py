import RPi.GPIO as GPIO
import ds18b20

disp_data = False

bound=1

def avg(nums):
	return sum(nums)/len(nums)

GPIO.setmode(GPIO.BCM)

def set(box,temp):
	if box.lower()=='inside':
		fan_pin=23
		heater_pin=5
		sensors=['28-000006949699','28-00000695c78d','28-000006dd46cf','28-00000695606a']
	elif box.lower()=='outside':
		fan_pin=24
		heater_pin=6
		sensors=['28-000006ddba65','28-000006954181','28-0000069500f1','28-000006de34f0']

	GPIO.setup(fan_pin,GPIO.OUT)
	GPIO.setup(heater_pin,GPIO.OUT)

	GPIO.output(fan_pin,True)

	try:
		while True:
			temp=avg(ds18b20.read(sensors))
			dtemp=temp_set-temp
			if dtemp<bound/2:
				GPIO.output(heater_pin,False)
			elif dtemp>-bound/2:
				GPIO.output(heater_pin,True)
			if disp_data:
				print str(bool(GPIO.input(heater_pin)))+"   "+str(temp)+" C"
	except KeyboardInterrupt:
		GPIO.output(heater_pin,False)
		GPIO.output(fan_pin,False)
		GPIO.cleanup()

if __name__=="__main__":
	while True:
		box=raw_input("Inside or Outside box? ")
		if box.lower()=='inside':
			break
		elif box.lower()=='outside':
			break
		else:
			print "Input not Inside or Outside."
	temp_set=input("Temperature setting: ");
	set(box.lower(),temp)
