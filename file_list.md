Three text files are present. These files were downloaded from the NCDC (National Climatic Data Center).

allstations.txt is a list of all the temperature recording stations. This is used by find_station.py.

dly-tmax-normal.txt is the average daily maximum temperatures for many locations in the US from 1981 to 2010. This file will be needed by simulation.py, but this file isn't completed currently.

dly-tmin-normal.txt is the same as the previous file, except it has average minimum temperatures. This file will also be needed by simulation.py.

Three python scripts are also here.

find_station.py is used to find the closest station to a given address. This file requires the geopy library. Example python code to use this function is below:
	from find_station import find_station
	station=find_station("5000 Abbey Way SE, Lacey, WA 98503")
	print station

switchtest.py is a script which can be used to turn the three GPIO pins used to control the furnace on and off for testing sake. It is included here mostly as an example of how to use the RPi.GPIO library.

simulation.py is a work in progress which will find the energy required with a given protocol for a given timeframe.
