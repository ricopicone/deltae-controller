import ds18b20
import time
import data_name
from experiment_time import etime

datafile=data_name.name()

inside=['28-000006949699','28-00000695c78d','28-000006dd46cf','28-00000695606a']
outside=['28-000006ddba65','28-000006954181','28-0000069500f1','28-000006de34f0']

while True
	out_temp=ds18b20.read(outside)
	in_temp=ds18b20.read(inside)
	file=open(datafile,'a')
	data=str(etime())+','+str(in_temp)+','+str(out_temp)+'\n'
	file.write(data)
	file.close()
	time.sleep(5)
