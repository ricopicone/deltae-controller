from geopy.geocoders.osm import Nominatim

def find_station(address):
	#This function finds the weather station that is closest to the address given
	lat,lon=find_latlon(address)  #Uses the find_latlon function to find the lat lon of the address
	file=open('allstations.txt','r')  #Opens the allstations text file which includes the lat lon of each station
	rawdata=file.readlines()  #Read the allstations file
	file.close()  #close allstations
	data=[]  #start the nested list with all the pertinent station data
	for line in rawdata:  #for each line of the allstations file
		data.append([line[0:11],float(line[12:20]),float(line[21:30])])  #convert the allstations data into the correct data types and organize it for easy use
	distance=[]  #start the distance list
	for line in data: #for each station
		delta_lat=lat-line[1] #find the change in latitude between the station and the address
		delta_lon=lon-line[2] #find the change in longitude
		delta=(delta_lat*delta_lat)+(delta_lon*delta_lon)  #Use pythagorean theorem to find the total distance, actually the square of the overall delta
		distance.append([delta,line[0]])  #add the delta and the station name to the distance list
	distance.sort()  #sort the distance list
	return distance[0][1]  #return the station which has the lowest delta

def find_latlon(address):
	geolocator=Nominatim()  #start the Noinatim geolocator
	location=geolocator.geocode(address)  #give the geolocator the address
	return location.latitude, location.longitude  #return the lat lon
