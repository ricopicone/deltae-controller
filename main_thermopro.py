from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.button import Button
from kivy.config import Config
from kivy.uix.popup import Popup
from kivy.uix.label import Label
from kivy.uix.image import Image
from kivy.clock import Clock
from kivy.properties import StringProperty

import time

Config.set('kivy','log_level','debug')
Config.write()

class IncrediblyCrudeClock(Label):
    def update(self, *args):
        self.text = time.asctime()

class Prototype(Widget):
    def OpenPopup(self,source):
        self.source=source
        sp=PopupOne(source)
        sp.open()
    def OpenPopup(self,source):
        self.source=source
        sp=PopupTwo(source)
        sp.open()
    def build(self):
        crudeclock = IncrediblyCrudeClock()
        Clock.schedule_interval(crudeclock.update, 1)
        return crudeclock
    def update(self, *args):
        self.text = time.asctime()
        
class PopupOne(Popup):    
    def __init__(self,source):
        super(PopupOne, self ).__init__()
        self.source=source
        auto_dismiss=False

class PopupTwo(Popup):    
    def __init__(self,source):
        super(PopupTwo, self ).__init__()
        self.source=source
        auto_dismiss=False

class thermoprototype(App):
    def build(self):
    	return Prototype()



if __name__ == '__main__':
	thermoprototype().run()
