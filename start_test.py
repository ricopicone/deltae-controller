import subprocess
import os
import signal

def start(temp_out,temp_in,ss_time)
	pid = []
	p = subprocess.Popen(["python","temp_control_in.py","outside",str(temp_out)])
	pid.append(p.pid)
	p = subprocess.Popen(["python","temp_control_in.py","inside",str(temp_in)])
	pid.append(p.pid)
	p = subprocess.Popen(["python","get_data.py"])
	pid.append(p.pid)
	return pid

def stop(pid)
	for id in pid:
		os.kill(id,signal.SIGTERM)
