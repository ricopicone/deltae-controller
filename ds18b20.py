import os

def list():
	sensors=os.listdir("/sys/bus/w1/devices/")
	sensors.remove("w1_bus_master1")
	return sensors

def read(serial):

	temps=[]

	if str(serial).count("[")==0&str(serial).count("(")==0:
		serial=[serial]

	for sensor in serial:
		f=open("/sys/bus/w1/devices/"+sensor+"/w1_slave","r")
		raw=f.readlines()
		f.close()
		check=raw[0].rstrip().split()[-1]
		if check=="YES":
			value=float(raw[1].rstrip().split()[-1].strip("t="))/1000
			temps.append(value)
		else:
			temps.append("Error")

	if len(temps)==1:
		return temps[0]
	else:
		return temps
