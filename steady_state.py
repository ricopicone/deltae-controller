import time
import experiment_time
import data_name
import sys

datafile=data_name.name()

sensitivity=0.2
thresh=6

def check(temp_target, time_ss):
	f=open(datafile,"r")
	rawdata=f.readlines()
	f.close()
	data=[]
	for line in rawdata:
		data.append(line.split(','))
	data.reverse()
	current_time=experiment_time.etime()
	data_limit=-1
	while True:
		data_limit+=1
		if data[data_limit][0]<current_time-time_ss:
			break
	data=data[0:data_limit]
	num_out_bound=0
	for line in data:
		if line[1]>temp_target+sensitivity:
			num_out_bound+=1
		elif line[1]<temp_target-sensitivity:
			num_out_bound+=1
	if num_out_bound<=thresh:
		return "ss"

if __name__=="__main__":
	print test(sys.argv[1],sys.argv[2])
