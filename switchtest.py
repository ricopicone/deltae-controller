import RPi.GPIO as GPIO  #import the GPIO library

GPIO.setmode(GPIO.BCM)  #set the mode to the Broadcom pin numbers

GPIO.setup(11,GPIO.OUT)  #set the correct pins as output
GPIO.setup(12,GPIO.OUT)
GPIO.setup(13,GPIO.OUT)

err=0  #if the input cannot be used this is set to true

while True:
	inst=raw_input("pin number or exit: ")  #take the input from the user
	if inst=="11":  #if the user types in 11, set pin to 11
		pin=11
	elif inst=="12":  #if input is 12, set pin to 12
		pin=12
	elif inst=="13":  #if 13, set as 13
		pin=13
	elif inst[0]=="e":  #if the first leter of the input is e, the first letter in exit, break from the loop
		break
	else:  #if none of the others are true, we can't proceed to the next step, so set err to True
		err=True
		print "error reading instruction"
	if not err:  #if err isn't true
		state=raw_input("on or off: ")  #ask whether to turn on or off
		if state=="on":  #if the user want to turn on
			GPIO.output(pin,True)  #set the pin to True, or on
		elif state=="off":  #if the user want to turn off
			GPIO.output(pin,False)  #set the pin to False, or off
		else: #if it isn't on or off don't do anything and tell the user
			print "error reading state"
	err=False  #clear the error

GPIO.cleanup()  #turns everything off and undoes the setup functions
